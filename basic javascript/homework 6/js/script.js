// Написать функцию filterBy(), которая будет принимать в себя 2 аргумента. Первый аргумент - массив,
// который будет содержать в себе любые данные, второй аргумент - тип данных.
// Функция должна вернуть новый массив, который будет содержать в себе все данные, которые были переданы в аргумент,
// за исключением тех, тип которых был передан вторым аргументом.
// То есть, если передать массив ['hello', 'world', 23, '23', null], и вторым аргументом передать 'string', то функция вернет массив [23, null].
//
debugger
function filterBy(arr, userDataType){
    return arr.filter(item => {
        if (userDataType === "null"){
            return item !== null;
        }else if (userDataType === "object" && item === null){
            return item === null;
        }else {
            return typeof item !== userDataType;
        }
    })
}

console.log(filterBy([1,[],null,{},2525],'null'));

//return arr.filter(item =>userDataType === "null"? item !== null
//         : userDataType === "object" && item === null ? item === null
//             : typeof item !== userDataType);