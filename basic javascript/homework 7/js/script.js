//Создать функцию, которая будет принимать на вход массив и опциональный второй аргумент parent - DOM-элемент,
// к которому будет прикреплен список (по дефолту должен быть document.body).
// Каждый из элементов массива вывести на страницу в виде пункта списка;
// Используйте шаблонные строки и метод map массива для формирования контента списка перед выведением его на страницу;

function getArray (arr, parent = document.body){
    const listItems = arr.map(item => `<li>${item}</li>`);
    return  parent.insertAdjacentHTML("afterbegin",`<ul>${listItems.join("")}</ul>`);
}

getArray(["hello", "World", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
















