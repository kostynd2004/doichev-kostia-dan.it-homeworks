$(document).ready(function(){


   $(window).scroll(function(){
      if (pageYOffset >= innerHeight){
         $(".arrow-up-container").show()
      }else {
         $(".arrow-up-container").hide()
         }
   })

   $(".arrow-up-container").click(function (){
      $("html, body").animate({
         scrollTop: 0
      });
   });

   $(".main-news-button").click(function () {
      $(".main-news-content").slideToggle(1000, () => {
         if ($(".main-news-content").css("display") === "none") {
            $(".main-news-button").text("Show Content");
         } else {
            $(".main-news-button").text("Hide Content");
         }
      });
   });
});