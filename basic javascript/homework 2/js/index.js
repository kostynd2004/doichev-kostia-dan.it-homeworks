
let userNumber = +prompt("Enter a number please","");

while((isNaN(userNumber))){
    userNumber = +prompt("Enter a number using digits, please","");
    if (!userNumber){
        break;
    }
}

    if (userNumber >= 5) {
        for (let i = 1; i <= userNumber; i++) {
                if (i < 5) {
                    continue;
                } else {
                    if (i % 5 === 0) {
                        console.log(i);
                    }
                }

            if (!userNumber) {
                break;
            }
        }
    } else {
        console.log("Sorry, no numbers");
    }


//      OPTIONAL
debugger
    let m = +prompt("Enter the first integer", "");
    let n = +prompt("Enter the second integer", "");

    while ((parseInt(n)) !== n || (parseInt(m)) !== m){
    alert("Enter an integer please");
        m = +prompt("Enter an integer please", "");
        n = +prompt("Enter an integer please", "");
        if ((parseInt(n)) === n && (parseInt(m)) === m){
            break;
        }
        if (!m && !n){
            break;
        }
    }

    if (m > n) {//перезаписываю значения переменных, если пользователь введет первое число больше, чем 2
        let smallestPrimeNumber = n;
        n = m;
        m = smallestPrimeNumber;
    }

    for (let i = m; i <= n; i++) {
        let signal = 0;

        for (j = 2; j < i; j++) {
            if (i % j === 0) {
                signal = 1;
                break;
            }
        }

        if (i > 1 && signal === 0) {
            console.log(i);
        }
    }
// AN Example I've seen in the Internet:

// // program to print prime numbers between the two numbers
// debugger
// // take input from the user
// const lowerNumber = parseInt(prompt('Enter lower number: '));
// const higherNumber = parseInt(prompt('Enter higher number: '));
//
// console.log(`The prime numbers between ${lowerNumber} and ${higherNumber} are:`);
//
// // looping from lowerNumber to higherNumber
// for (let i = lowerNumber; i <= higherNumber; i++) {// генерирует числа от lowerNumber(2) до higherNumber(18),
//     let flag = 0;// присваивает переменной flag значение 0
//
//     // looping through 2 to user input number
//     for (let j = 2; j < i; j++) {
//
//         if (i % j === 0) {//если число i делится без остатка на j, то flag = 1 и происходит break
//             flag = 1;
//             break;
//         }
//     } //MY OWN EXPLANATION ===> // переменная j каждый раз получает значение 2, если lowerNumber = 2, то условие этого цикла не выполняется,
//     // грубо говоря - break , дальше интерпретатор идёт на строку с оператором if и видит, что 2 условия === true, потому что цикл j брейкнулся
//     // и у переменной flag осталось значение 0. Потом интерпретатор возвращается к циклу i и увеличивает ёё значение на 1 ,
//     // видя , что условие верно , i всё ёще меньше за higherNumber, интерпретатор присваивает переменной flag значение 0,
//     //дальше он идет к циклу j и видит, что условие верно j(2) < i(3), поэтому он переходит к условию, но оно ложное, поэтому он двигается дальше
//     // и увеличивает j на 1, в результате условие цикла === false  и происходит break.Интерпретатор выходит из цикла
//     // и переходит к строке с оператором if и видит, что выражение === true, поэтому выводит i(3) в консоль.
//     //После этой операции он опят переходит к циклу i, где i === 4, оно все ещё меньше higherNumber(18),дальше выполняется код цикла j ,
//     // где j === 2.Она меньше i и поэтому можно переходить к телу цикла, а в теле написано , что если
//     // i делится нацело( без остатка) на j, то переменной flag присваивается значение 1 и происходит break, интерпретатор
//     // выходит из цикла, переходит к if ниже и видит, что условие if (i > 1 && flag === 0) ложное, потому что flag === 0 ===> false
//     //поэтому i со значением 4 не выводится в консоль
//
//     // if number greater than 1 and not divisible by other numbers если и то и то === true , тогда i будет выведена в консоль
//     if (i > 1 && flag === 0) {
//         console.log(i);
//     }
// }